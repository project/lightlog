<?php

/**
 * Allows modules to alter watchdog before it's logged by lightlog in database.
 *
 * Return empty if log hasn't to be logged
 *
 * @param $log_entry
 *   The array of log data entry.
 * @return $log_entry
 *   The array of log data entry edited.
 */
function hook_lightlog(array $log_entry) {
  return $log_entry;
}
