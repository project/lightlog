<?php

/**
 * @file
 * Administrative page callbacks for the lightlog module.
 */

/**
 * Menu callback; displays a listing of log messages.
 *
 * Messages are truncated at 56 chars. Full-length message could be viewed at
 * the message details page.
 *
 * @ingroup logging_severity_levels
 */
function lightlog_overview() {
  $filter = lightlog_build_filter_query();
  $rows = array();
  $classes = array(
    WATCHDOG_DEBUG     => 'lightlog-debug',
    WATCHDOG_INFO      => 'lightlog-info',
    WATCHDOG_NOTICE    => 'lightlog-notice',
    WATCHDOG_WARNING   => 'lightlog-warning',
    WATCHDOG_ERROR     => 'lightlog-error',
    WATCHDOG_CRITICAL  => 'lightlog-critical',
    WATCHDOG_ALERT     => 'lightlog-alert',
    WATCHDOG_EMERGENCY => 'lightlog-emerg',
  );

  $build['lightlog_filter_form'] = drupal_get_form('lightlog_filter_form');
  $build['lightlog_clear_log_form'] = drupal_get_form('lightlog_clear_log_form');

  $header = array(
    '', // Icon column.
    array('data' => t('Type'), 'field' => 'w.type'),
    array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
    t('Message'),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Operations')),
  );

  $query = db_select('lightlog', 'w')->extend('PagerDefault')->extend('TableSort');
  $query->leftJoin('users', 'u', 'w.uid = u.uid');
  $query
    ->fields('w', array('wid', 'uid', 'severity', 'type', 'timestamp', 'message', 'variables', 'link'))
    ->addField('u', 'name');
  if (!empty($filter['where'])) {
    $query->where($filter['where'], $filter['args']);
  }
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  foreach ($result as $lightlog) {
    $rows[] = array('data' =>
      array(
        // Cells
        array('class' => 'icon'),
        t($lightlog->type),
        format_date($lightlog->timestamp, 'short'),
        theme('lightlog_message', array('event' => $lightlog, 'link' => TRUE)),
        theme('username', array('account' => $lightlog)),
        filter_xss($lightlog->link),
      ),
      // Attributes for tr
      'class' => array(drupal_html_class('lightlog-' . $lightlog->type), $classes[$lightlog->severity]),
    );
  }

  $build['lightlog_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'admin-lightlog'),
    '#empty' => t('No log messages available.'),
  );
  $build['lightlog_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; generic function to display a page of the most frequent events.
 *
 * Messages are not truncated because events from this page have no detail view.
 *
 * @param $type
 *   type of lightlog events to display.
 */
function lightlog_top($type) {

  $header = array(
    array('data' => t('Count'), 'field' => 'count', 'sort' => 'desc'),
    array('data' => t('Message'), 'field' => 'message')
  );
  $count_query = db_select('lightlog');
  $count_query->addExpression('COUNT(DISTINCT(message))');
  $count_query->condition('type', $type);

  $query = db_select('lightlog', 'w')->extend('PagerDefault')->extend('TableSort');
  $query->addExpression('COUNT(wid)', 'count');
  $query = $query
    ->fields('w', array('message', 'variables'))
    ->condition('w.type', $type)
    ->groupBy('message')
    ->groupBy('variables')
    ->limit(30)
    ->orderByHeader($header);
  $query->setCountQuery($count_query);
  $result = $query->execute();

  $rows = array();
  foreach ($result as $lightlog) {
    $rows[] = array($lightlog->count, theme('lightlog_message', array('event' => $lightlog)));
  }

  $build['lightlog_top_table']  = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No log messages available.'),
  );
  $build['lightlog_top_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; displays details about a log message.
 */
function lightlog_event($id) {
  $severity = watchdog_severity_levels();
  $result = db_query('SELECT w.*, u.name, u.uid FROM {lightlog} w INNER JOIN {users} u ON w.uid = u.uid WHERE w.wid = :id', array(':id' => $id))->fetchObject();
  if ($lightlog = $result) {
    $rows = array(
      array(
        array('data' => t('Type'), 'header' => TRUE),
        t($lightlog->type),
      ),
      array(
        array('data' => t('Date'), 'header' => TRUE),
        format_date($lightlog->timestamp, 'long'),
      ),
      array(
        array('data' => t('User'), 'header' => TRUE),
        theme('username', array('account' => $lightlog)),
      ),
      array(
        array('data' => t('Location'), 'header' => TRUE),
        l($lightlog->location, $lightlog->location),
      ),
      array(
        array('data' => t('Referrer'), 'header' => TRUE),
        l($lightlog->referer, $lightlog->referer),
      ),
      array(
        array('data' => t('Message'), 'header' => TRUE),
        theme('lightlog_message', array('event' => $lightlog)),
      ),
      array(
        array('data' => t('Severity'), 'header' => TRUE),
        $severity[$lightlog->severity],
      ),
      array(
        array('data' => t('Hostname'), 'header' => TRUE),
        check_plain($lightlog->hostname),
      ),
      array(
        array('data' => t('Operations'), 'header' => TRUE),
        $lightlog->link,
      ),
    );
    $build['lightlog_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#attributes' => array('class' => array('lightlog-event')),
    );
    return $build;
  }
  else {
    return '';
  }
}

/**
 * Build query for lightlog administration filters based on session.
 */
function lightlog_build_filter_query() {
  if (empty($_SESSION['lightlog_overview_filter'])) {
    return;
  }

  $filters = lightlog_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['lightlog_overview_filter'] as $key => $filter) {
    $filter_where = array();
    foreach ($filter as $value) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if (!empty($filter_where)) {
      $where[] = '(' . implode(' OR ', $filter_where) . ')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}


/**
 * List lightlog administration filters that can be applied.
 */
function lightlog_filters() {
  $filters = array();

  foreach (_lightlog_get_message_types() as $type) {
    $types[$type] = t($type);
  }

  if (!empty($types)) {
    $filters['type'] = array(
      'title' => t('Type'),
      'where' => "w.type = ?",
      'options' => $types,
    );
  }

  $filters['severity'] = array(
    'title' => t('Severity'),
    'where' => 'w.severity = ?',
    'options' => watchdog_severity_levels(),
  );

  return $filters;
}

/**
 * Returns HTML for a log message.
 *
 * @param $variables
 *   An associative array containing:
 *   - event: An object with at least the message and variables properties.
 *   - link: (optional) Format message as link, event->wid is required.
 *
 * @ingroup themeable
 */
function theme_lightlog_message($variables) {
  $output = '';
  $event = $variables['event'];
  // Check for required properties.
  if (isset($event->message) && isset($event->variables)) {
    // Messages without variables or user specified text.
    if ($event->variables === 'N;') {
      $output = $event->message;
    }
    // Message to translate with injected variables.
    else {
      $output = t($event->message, unserialize($event->variables));
    }
    if ($variables['link'] && isset($event->wid)) {
      // Truncate message to 56 chars.
      $output = truncate_utf8(filter_xss($output, array()), 56, TRUE, TRUE);
      $output = l($output, 'admin/reports/lightlog/event/' . $event->wid, array('html' => TRUE));
    }
  }
  return $output;
}

/**
 * Return form for lightlog administration filters.
 *
 * @ingroup forms
 * @see lightlog_filter_form_submit()
 * @see lightlog_filter_form_validate()
 */
function lightlog_filter_form($form) {
  $filters = lightlog_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($_SESSION['lightlog_overview_filter']),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filter['options'],
    );
    if (!empty($_SESSION['lightlog_overview_filter'][$key])) {
      $form['filters']['status'][$key]['#default_value'] = $_SESSION['lightlog_overview_filter'][$key];
    }
  }

  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['lightlog_overview_filter'])) {
    $form['filters']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from lightlog administration filter form.
 */
function lightlog_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['type']) && empty($form_state['values']['severity'])) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from lightlog administration filter form.
 */
function lightlog_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = lightlog_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['lightlog_overview_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['lightlog_overview_filter'] = array();
      break;
  }
  return 'admin/reports/lightlog';
}

/**
 * Return form for lightlog clear button.
 *
 * @ingroup forms
 * @see lightlog_clear_log_submit()
 */
function lightlog_clear_log_form($form) {
  $form['lightlog_clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear log messages'),
    '#description' => t('This will permanently remove the log messages from the database.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['lightlog_clear']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log messages'),
    '#submit' => array('lightlog_clear_log_submit'),
  );

  return $form;
}

/**
 * Submit callback: clear database with log messages.
 */
function lightlog_clear_log_submit() {
  $_SESSION['lightlog_overview_filter'] = array();
  db_delete('lightlog')->execute();
  drupal_set_message(t('Database log cleared.'));
}

function lightlog_settings() {
  $form = array();

  // List types to allow.
  $form['lightlog_allowed_types'] = array(
    '#title' => t('Allowed types'),
    '#description' => t('Select types which are allowed to be stored in lightlog.'),
    '#type' => 'checkboxes',
    '#options' => variable_get('lightlog_types_list', array()),
    '#default_value' => variable_get('lightlog_allowed_types', array()),
  );

  // List severities to allow.
  $form['lightlog_allowed_severity'] = array(
    '#title' => t('Allowed severities'),
    '#description' => t('Select severities which are allowed to be stored in lightlog.'),
    '#type' => 'checkboxes',
    '#options' => watchdog_severity_levels(),
    '#default_value' => variable_get('lightlog_allowed_severity', _lightlog_default_severities()),
  );

  return system_settings_form($form);
}